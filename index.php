<?php

require_once('TemplatesList.php');

?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="stylesheet" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css"/>

    <!--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    -->

    <link rel="icon" href="favicon.ico">

    <link rel="stylesheet" href="style.css"/>
</head>
<body class="container">
<img class="logo" src="img/header-logo.png"/>

<div class="doctemplate-chooser">
    <h1>Выберите тип документа:</h1>

    <div class="doctemplates clearfix">
        <?php
        foreach ($Templates as $template) {
            ?>
            <a class="doctemplate" href="form.php?t=<?php echo get_class($template) ?>">
                <img src="<?php echo $template->getThumbnailURL() ?>"/>
                <?php echo $template->getTitle() ?>
            </a>
            <?php
        }
        ?>
    </div>
</div>
</body>
</html>
