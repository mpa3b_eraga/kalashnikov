<!DOCTYPE html>
<html>
<?php
$templateName = $_REQUEST['t'];
require_once('templates/' . $templateName . '.php');
$template = new $templateName();
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <link rel="stylesheet" href="http://yui.yahooapis.com/3.18.1/build/cssreset/cssreset-min.css"/>

    <!--
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    -->

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>

    <link rel="stylesheet" href="/lib/jquery-ui-1.11.2.custom/jquery-ui.min.css"/>
    <script src="/lib/jquery-ui-1.11.2.custom/jquery-ui.min.js"></script>
    <script src="/lib/jquery-ui-datepicker-ru.js"></script>

    <script src="/lib/ckeditor/ckeditor.js"></script>
    <script src="/lib/ckeditor/adapters/jquery.js"></script>

    <link rel="stylesheet" href="/style.css"/>
    <script src="/script.js"></script>
</head>
<body class="container">
<img class="logo" src="img/header-logo.png"/>

<div class="header">
    <div class="template-selector">
        <h1><?php echo $template->getShortTitle() ?></h1>
        <a href="/" class="button">Изменить</a>
    </div>
    <div class="format-selector">
        <div class="format-button format-pdf active" value="pdf"></div>
        <div class="format-button format-rtf" value="rtf"></div>
        <div class="format-button format-docx" value="docx"></div>
    </div>

    <button class="download">Получить документ</button>
</div>

<form name="document" method="post" target="_blank" action="doc.php">
    <input type="hidden" name="template" value="<?php echo get_class($template) ?>"/>
    <input type="hidden" name="format" value="pdf"/>

    <?php
    echo $template->getHTMLForm();
    ?>
</form>

<div class="footer">
    <a class="help-link" href="fakelink">Справочная</a>
    © 2015
</div>
</body>
</html>
