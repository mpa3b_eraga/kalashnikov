<?php

// ini_set('display_startup_errors', 1);
// ini_set('display_errors', 1);
// error_reporting(-1);
// 
// header('Content-Type: text/plain');
// 
// $html = '
// <p>Hello, World</p>
// <ol>
//     <li>First</li>
//     <li>Second
//         <ol>
//             <li>Third</li>
//         </ol>
//         <ul>
//             <li>Fourth</li>
//         </ul>
//     </li>
//     <li>Fifth</li>
// </ol>
// <p>Footer</p>
// ';




class SimpleHTMLParser
{
    public function __construct()
    {
        $this->xml = xml_parser_create();
        xml_set_object($this->xml, $this);

        xml_set_element_handler($this->xml, 'start_element', 'end_element');
        xml_set_character_data_handler($this->xml, 'character_data');
    }


    public function parse($html, $phpWord, $container, $fontStyle, $parStyle)
    {
        $this->_LOGGING = false;

        if ($this->_LOGGING)
            $this->indent = 0;
        $html = preg_replace('/<br[ \/]*>/', "\n", $html);
        $this->phpWord = $phpWord;
        $this->container = $container;
        $this->fontStyle = $fontStyle;
        $this->parStyle = $parStyle;

        $this->tagsStack = array();

        $this->listDepth = -1;
        $this->listStyle = array();
        $this->addListItemArgs = array();

        $this->listNums = array();

        $this->currentTagText = null;

        xml_parse($this->xml, '<html>' . html_entity_decode($html) . '</html>');
    }

    private function lastTag()
    {
        if (count($this->tagsStack) > 0)
            return $this->tagsStack[count($this->tagsStack) - 1];
        return null;
    }

    private function start_element($parser, $tag, $attributes)
    {
        if ($this->_LOGGING) {
            $this->indent();
            echo "s $tag\n";
            $this->indent++;
        }

        switch ($tag) {
            case 'OL':
            case 'UL':
                if (trim($this->currentTagText))
                    $this->addListItemArgs[] = array($this->currentTagText, $this->listDepth, $this->listStyle[$this->listDepth]);

                $this->listDepth++;
                $this->listStyle[$this->listDepth] = $tag == 'OL' ? 'decimal' : 'bullet';

                break;
        }

        $this->tagsStack[] = $tag;
        $this->currentTagText = '';
    }

    private function indent()
    {
        for ($i = 0; $i < $this->indent; $i++)
            echo '   ';
    }

    private function end_element($parser, $tag)
    {
        if ($this->_LOGGING) $this->indent--;

        switch ($tag) {
            case 'P':
                $this->listNums = array();
                if ($this->_LOGGING) echo "addText($this->currentTagText)\n";
                $this->container->addText($this->currentTagText, $this->fontStyle, $this->parStyle);
                break;

            case 'LI':
                if (trim($this->currentTagText))
                    $this->addListItemArgs[] = array($this->currentTagText, $this->listDepth, $this->listStyle[$this->listDepth]);
                break;

            case 'OL':
            case 'UL':
                $this->listDepth--;

                if ($this->listDepth == -1) {
                    $styleName = "list_" . rand(0, 1e6);

                    $levels = array();
                    for ($level = 0; $level < count($this->listStyle); $level++) {
                        $levelStyle = array(
                            'format' => $this->listStyle[$level],
                            'align' => 'left',
                            'left' => 810,
                            'hanging' => 80,
                            'tabPos' => 810,
                        );
                        if ($this->listStyle[$level] == 'decimal') {
                            $levelStyle['text'] = '';
                            for ($n = 1; $n < $level + 2; $n++) {
                                $levelStyle['text'] .= '%' . $n . '.';
                            }
                        } else {
                            switch ($level % 3) {
                                case 0:
                                    $levelStyle['text'] = '•';
                                    break;
                                case 1:
                                    $levelStyle['text'] = '◦';
                                    break;
                                case 2:
                                    $levelStyle['text'] = '▪';
                                    break;
                            }
                        }
                        $levels[] = $levelStyle;
                    }

                    if ($this->_LOGGING) {
                        echo "addNumberingStyle($styleName, ";
                        var_dump(array(
                            'type' => 'multilevel',
                            'levels' => $levels,
                        ));
                        echo ");\n";
                    }

                    $this->phpWord->addNumberingStyle(
                        $styleName,
                        array(
                            'type' => 'multilevel',
                            'levels' => $levels,
                        )
                    );

                    $this->container->addText('', array('size' => 4), array(
                        'align' => 'justify',
                        'spaceAfter' => 0,
                        'lineHeight' => 1,
                        'spaceBefore' => 0,
                    ));
                    foreach ($this->addListItemArgs as $args) {
                        if (!isset($this->listNums[$args[1]])) {
                            $this->listNums[$args[1]] = 0;
                        }
                        if ($args[2] == 'decimal') {
                            $this->listNums[$args[1]] += 1;
                            foreach ($this->listNums as $n => $num) {
                                if ($n > $args[1]) {
                                    $this->listNums[$n] = 0;
                                }
                            }
                            $num = '';
                            for($i=0; $i<$args[1]+1; $i++) {
                                $num .= $this->listNums[$i].'.';
                            }
                            $num = '       '.$num;
                        } else {
                            $num = '-';
                        }

                        $font = [
                            'name' => 'Verdana',
                            'size' => 11
                        ];
                        $par = [
                            'align' => 'left',
                            'spaceAfter' => 200,
                            'lineHeight' => 1.15
                        ];

                        $this->container->addText($num.' '.$args[0], 'body', 'body_p');

//                        if ($this->_LOGGING) echo "addListItem($args[0] $args[1], $styleName)\n";
//                        $this->container->addListItem($args[0], $args[1], $this->fontStyle, $styleName, $this->parStyle);

                    }
                    $this->container->addText('', array('size' => 4), array(
                        'align' => 'justify',
                        'spaceAfter' => 0,
                        'lineHeight' => 1,
                        'spaceBefore' => 0,
                    ));

                    $this->addListItemArgs = array();
                }
                break;
        }

        array_pop($this->tagsStack);
        $this->currentTagText = '';
    }

    private function character_data($parser, $cdata)
    {
        if (!trim($cdata)) return;
        $cdata = trim($cdata);

        $this->currentTagText .= $cdata;

        if ($this->_LOGGING) {
            $this->indent();
            echo "c $cdata\n";
        }
    }
}

// (new SimpleHTMLParser())->parse($html);
