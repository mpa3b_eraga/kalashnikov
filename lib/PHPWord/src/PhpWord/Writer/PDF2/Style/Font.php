<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Style;

use PhpOffice\PhpWord\Style;


class Font
{

    public function __construct($fontStyle)
    {
        if (is_string($fontStyle))
            $this->fontStyle = Style::getStyle($fontStyle);
        else
            $this->fontStyle = $fontStyle;
    }


    public function apply($pdf)
    {
        if ($this->fontStyle == null) return;

        $flags = '';
        if ($this->fontStyle->isBold()) $flags .= 'B';
        if ($this->fontStyle->isItalic()) $flags .= 'I';
        $pdf->SetFont($this->fontStyle->getName(), $flags, $this->fontStyle->getSize());

        if ($this->fontStyle->getColor() != null)
        {
            $c = $this->fontStyle->getColor();
            if (strlen($c) == 6)
            {
                $r = hexdec(substr($c, 0, 2));
                $g = hexdec(substr($c, 2, 2));
                $b = hexdec(substr($c, 4, 2));
                $pdf->SetTextColorArray(array($r, $g, $b));
            }
        }
    }

}
