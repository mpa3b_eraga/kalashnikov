<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Style;

use PhpOffice\PhpWord\Writer\PDF2\Size;
use PhpOffice\PhpWord\Style\Alignment;
use PhpOffice\PhpWord\Style;


class Paragraph
{

    public function __construct($parStyle)
    {
        if (is_string($parStyle))
            $this->parStyle = Style::getStyle($parStyle);
        else
            $this->parStyle = $parStyle;
    }


    public function apply($pdf, $trailingMargin)
    {
        if ($this->parStyle == null) return;

        $spaceBefore = max($trailingMargin, Size::tocm($this->parStyle->getSpaceBefore()));

        $pdf->SetXY(
            $pdf->GetX(),
            $pdf->GetY() - $trailingMargin + $spaceBefore
        );

        $lineHeight = $this->parStyle->getLineHeight();
        if ($lineHeight == null) $lineHeight = 1;
        $pdf->setCellHeightRatio(
            $lineHeight + 0.25
        );
    }

    public function applyAfter($pdf, &$trailingMargin)
    {
        if ($this->parStyle == null) return;

        $spaceAfter = Size::tocm($this->parStyle->getSpaceAfter());

        $trailingMargin = $spaceAfter;

        $pdf->SetXY(
            $pdf->GetX(),
            $pdf->GetY() + $spaceAfter
        );
    }


    public function align()
    {
        if ($this->parStyle == null) return 'L';

        switch ($this->parStyle->getAlign())
        {
            case Alignment::ALIGN_RIGHT: return 'R';
            case Alignment::ALIGN_CENTER: return 'C';
            case Alignment::ALIGN_BOTH:
            case Alignment::ALIGN_JUSTIFY: return 'J';
            default: return 'L';
        }
    }


    public function keepNext()
    {
        if ($this->parStyle == null)
            return null;
        return $this->parStyle->isKeepNext();
    }

}
