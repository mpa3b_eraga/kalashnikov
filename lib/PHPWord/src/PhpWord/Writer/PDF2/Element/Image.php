<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Writer\PDF2\Element\AbstractElement;
use PhpOffice\PhpWord\Writer\PDF2\Size;

use PhpOffice\PhpWord\Element\Image as ImageElement;
use PhpOffice\PhpWord\Style\Alignment;


class Image extends AbstractElement
{

    public function write($pdf, &$trailingMargin, $width = null)
    {
        $e = $this->getElement();
        $style = $e->getStyle();


        if ($e->getSourceType() != ImageElement::SOURCE_LOCAL)
            return;

        switch ($style->getAlign())
        {
            case Alignment::ALIGN_RIGHT: $palign = 'R'; break;
            case Alignment::ALIGN_CENTER: $palign = 'C'; break;
            default: $palign = 'L'; break;
        }


        $pdf->Image(
            $e->getSource(),
            '',
            '',
            $style->getWidth()/96*2.5,
            $style->getHeight()/96*2.5,
            '',
            '',
            'N',
            false,
            300,
            $palign,
            false
        );
    }

}
