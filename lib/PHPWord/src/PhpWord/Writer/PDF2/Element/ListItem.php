<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Style;
use PhpOffice\PhpWord\Writer\PDF2\Size;
use PhpOffice\PhpWord\Writer\PDF2\Style\Paragraph;
use PhpOffice\PhpWord\Writer\PDF2\Style\Font;


class ListItem extends AbstractElement
{

    public function write($pdf, &$trailingMargin, $width = null, $listIndexes = array())
    {
        $e = $this->getElement();

        $style = Style::getStyle($e->getStyle()->getNumStyle());
        $levelStyle = $style->getLevels()[$e->getDepth()];

        $pStyle = new Paragraph($e->getTextObject()->getParagraphStyle());
        $fStyle = new Font($e->getTextObject()->getFontStyle());

        $fStyle->apply($pdf);
        $pStyle->apply($pdf, $trailingMargin);

        $x = $pdf->GetX();

        $pdf->SetXY($pdf->getMargins()['left'], $pdf->GetY());

        $bulletText = $levelStyle->getText();
        switch ($levelStyle->getFormat())
        {
            case 'decimal':
                for ($i = 0; $i < count($listIndexes); $i++)
                    $bulletText = str_replace('%' . ($i+1), $listIndexes[$i], $bulletText);
                break;
        }

        $pdf->Cell(
            Size::tocm($levelStyle->getLeft() - $levelStyle->getHanging()),
            0,
            $bulletText,
            0,
            0,
            'R'
        );


        $pdf->SetXY(
            $x + Size::tocm($levelStyle->getLeft()),
            $pdf->GetY()
        );

        $pdf->MultiCell(
            $width = null ? 0 : $width,
            0,
            $this->getText(),
            0,
            $pStyle->align(),
            false,
            1
        );

        $pdf->SetXY($x, $pdf->GetY());

        $pStyle->applyAfter($pdf, $trailingMargin);
    }


    protected function getText()
    {
        $text = $this->getElement()->getTextObject()->getText();
        if (strlen($text) > 0 && $text[strlen($text)-1] != "\n")
            $text .= "\n";
        return $text;
    }

}
