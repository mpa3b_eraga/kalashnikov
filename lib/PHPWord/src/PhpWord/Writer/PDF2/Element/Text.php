<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Writer\PDF2\Element\AbstractElement;
use PhpOffice\PhpWord\Writer\PDF2\Size;
use PhpOffice\PhpWord\Writer\PDF2\Style\Paragraph;
use PhpOffice\PhpWord\Writer\PDF2\Style\Font;


class Text extends AbstractElement
{

    public function write($pdf, &$trailingMargin, $width = null)
    {
        $e = $this->getElement();

        $pStyle = new Paragraph($e->getParagraphStyle());
        $fStyle = new Font($e->getFontStyle());

        $fStyle->apply($pdf);
        $pStyle->apply($pdf, $trailingMargin);

        $x = $pdf->GetX();

        $pdf->MultiCell(
            $width == null ? 0 : $width, // w
            0,                           // h
            $this->getText($pdf),        // text
            0,                           // border
            $pStyle->align(),            // align
            false,                       // fill
            1                            // ln
        );

        $pdf->SetXY($x, $pdf->GetY());

        $pStyle->applyAfter($pdf, $trailingMargin);
    }


    protected function getText($pdf)
    {
        $text = $this->getElement()->getText();
        if (strlen($text) > 0 && $text[strlen($text)-1] != "\n")
            $text .= "\n";
        return $text;
    }

}
