<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;


class PreserveText extends Text
{

    protected function getText($pdf)
    {
        $text = '';
        foreach ($this->getElement()->getText() as $part)
        {
            if ($part[0] == '{')
            {
                if (preg_replace('/^\{\s*|\s*}$/', '', $part) == 'PAGE')
                {
                    $text .= $pdf->getPage();
                    continue;
                }
            }

            $text .= $part;
        }

        return $text;
    }

}
