<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Style;
use PhpOffice\PhpWord\Element\ListItem;


class Container extends AbstractElement
{

    protected $namespace = 'PhpOffice\\PhpWord\\Writer\\PDF2\\Element';

    private $listIndexes = array();

    public function write($pdf, &$trailingMargin, $width = null, $listIndexes = null)
    {
        $elements = $this->getElement()->getElements();

        $this->listIndexes = array();

        for ($i = 0; $i < count($elements); $i++)
        {
            $element = $elements[$i];

            if ($element instanceof ListItem)
            {
                if (isset($this->listIndexes[$element->getDepth()]))
                {
                    $this->listIndexes[$element->getDepth()]++;
                    array_splice($this->listIndexes, $element->getDepth()+1);
                }
                else
                    $this->listIndexes[$element->getDepth()] = 1;
            }
            else
                $this->listIndexes = array();

            if ($this->hasKeepNextFlag($element))
            {
                $tmp_pdf = clone($pdf);
                $ok = $this->writeElementCheckingKeepNext($tmp_pdf, $elements, $i, $width);

                if (! $ok)
                    $pdf->AddPage();

                $this->writeElement($pdf, $element, $trailingMargin, $width);
            }
            else
                $this->writeElement($pdf, $element, $trailingMargin, $width);
        }
    }


    protected function writeElement($pdf, $element, &$trailingMargin, $width = null)
    {
        $elementClass = substr(get_class($element), strrpos(get_class($element), '\\') + 1);

        $writerClass = $this->namespace . '\\' . $elementClass;

        if (class_exists($writerClass))
        {
            if ($element instanceof ListItem)
                (new $writerClass($element))->write($pdf, $trailingMargin, $width, $this->listIndexes);
            else
                (new $writerClass($element))->write($pdf, $trailingMargin, $width);
        }
    }


    protected function writeElementCheckingKeepNext($pdf, $elements, $index, $width = null)
    {
        if ($index >= count($elements))
            return true;

        $trailingMargin = 0;

        $e = $elements[$index];

        if ($this->hasKeepNextFlag($e))
        {
            $page_no = $pdf->getPage();
            $this->writeElement($pdf, $e, $trailingMargin, $width);
            if ($pdf->getPage() != $page_no || $pdf->getPageHeight() - $pdf->GetY() - $pdf->getMargins()['bottom'] < $pdf->getCellHeight($pdf->getFontSize()))
                return false;

            return $this->writeElementCheckingKeepNext($pdf, $elements, $index+1, $width);
        }
        else
        {
            $this->writeElement($pdf, $e, $trailingMargin, $width);
            return true;
        }
    }


    protected function hasKeepNextFlag($element)
    {
        if (! method_exists($element, 'getParagraphStyle'))
            return false;

        $pStyle = $element->getParagraphStyle();
        if (is_string($pStyle))
            $pStyle = Style::getStyle($pStyle);
        if (is_object($pStyle))
            return $pStyle->isKeepNext();
        return false;
    }

}
