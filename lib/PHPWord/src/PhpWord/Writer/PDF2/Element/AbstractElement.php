<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Element\AbstractElement as Element;


abstract class AbstractElement
{

    private $element;

    abstract public function write($pdf, &$trailingMargin, $width = null);

    public function __construct(Element $element)
    {
        $this->element = $element;
    }


    public function getElement()
    {
        return $this->element;
    }
}
