<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Writer\PDF2\Size;
use PhpOffice\PhpWord\Style\Section as SectionStyle;


class Section extends Container
{

    public function write($pdf, &$trailingMargin, $width = null)
    {
        $section = $this->getElement();
        $style = $section->getSettings();

        $pdf->setSection($section);

        $pdf->setPageOrientation(
            $style->getOrientation() == SectionStyle::ORIENTATION_LANDSCAPE ? 'L' : 'P',
            true,
            Size::tocm($style->getMarginBottom())
        );
        $pdf->SetMargins(
            Size::tocm($style->getMarginLeft()),
            Size::tocm($style->getMarginTop()),
            Size::tocm($style->getMarginRight())
        );

        $pdf->AddPage();

        parent::write($pdf, $trailingMargin, $width);
    }

}
