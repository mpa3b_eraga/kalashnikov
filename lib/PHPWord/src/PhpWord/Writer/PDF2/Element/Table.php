<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Writer\PDF2\Size;
use PhpOffice\PhpWord\Writer\PDF2\Element\Container;


class Table extends AbstractElement
{

    public function write($pdf, &$trailingMargin, $width = null)
    {
        $table = $this->getElement();
        $style = $table->getStyle();

        $trailingMargin = 0;

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $inline = true;

        $position = $style ? $style->getPosition() : null;

        if ($position != null)
        {
            if ($position['tblpYSpec'] == 'bottom')
            {
                $height = $this->calcHeight($pdf);

                if ($position['vertAnchor'] == 'margin')
                {
                    $y = $pdf->getPageHeight() - $pdf->getMargins()['bottom'] - $height;
                    $inline = false;
                }
            }
        }


        if ($inline)
        {
            if ($pdf->GetY() + $this->calcHeight($pdf) > $pdf->getPageHeight() - $pdf->getMargins()['bottom'])
                $pdf->AddPage();
            $this->writeTable($pdf);
        }
        else
        {
            $orig_x = $x; $orig_y = $y;
            $pdf->SetXY($x, $y);
            $this->writeTable($pdf);
            $pdf->SetXY($orig_x, $orig_y);
        }
    }


    public function writeTable($pdf)
    {
        foreach ($this->getElement()->getRows() as $row)
            $this->writeRow($pdf, $row);
    }


    public function calcHeight($pdf)
    {
        $pdf->startTransaction();
        $pdf->SetY(0);
        $this->writeTable($pdf);
        $height = $pdf->GetY();
        $pdf->rollbackTransaction(true);

        return $height;
    }


    public function writeRow($pdf, $row)
    {
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $maxh = 0;
        foreach ($row->getCells() as $cell)
        {
            $h = $this->writeCell($pdf, $cell);
            $maxh = max($maxh, $h);
        }

        $pdf->SetXY($x, $y + $maxh);
    }

    public function writeCell($pdf, $cell)
    {
        $x = $pdf->GetX();
        $y = $pdf->GetY();
        // $p = $pdf->getPage();
        // $apb = $pdf->getAutoPageBreak();
        // $apbm = $pdf->getBreakMargin();
        // $pdf->setAutoPageBreak(false);

        $w = Size::tocm($cell->getWidth());
        $tm = 0;
        (new Container($cell))->write($pdf, $tm, $w);

        $h = $pdf->GetY() - $y;
        // $pdf->setPage($p);
        // $pdf->setAutoPageBreak($apb, $apbm);
        $pdf->SetXY($x + $w, $y);

        return $h;
    }

}
