<?php

namespace PhpOffice\PhpWord\Writer\PDF2\Element;

use PhpOffice\PhpWord\Writer\PDF2\Style\Paragraph;


class TextRun extends AbstractElement
{

    public function write($pdf, &$trailingMargin, $width = null)
    {
        $e = $this->getElement();

        $pStyle = new Paragraph($e->getParagraphStyle());

        $pStyle->apply($pdf, $trailingMargin);

        $x = $pdf->GetX();

        (new Container($e))->write($pdf, $trailingMargin);

        $pdf->SetXY($x, $pdf->GetY());

        $pStyle->applyAfter($pdf, $trailingMargin);
    }

}
