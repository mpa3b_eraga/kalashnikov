<?php

namespace PhpOffice\PhpWord\Writer\PDF2;

class Size
{

    public static function tocm($twips)
    {
        return $twips / 565.217;
    }

}
