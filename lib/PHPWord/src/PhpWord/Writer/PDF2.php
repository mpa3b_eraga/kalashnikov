<?php

namespace PhpOffice\PhpWord\Writer;

require_once 'lib/tcpdf/tcpdf.php';

use PhpOffice\PhpWord\Element\Footer;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Writer\PDF2\Element\Container;
use PhpOffice\PhpWord\Writer\PDF2\Element\Section;
use PhpOffice\PhpWord\Writer\PDF2\Size;


class MyPDF extends \TCPDF
{

    private $section = null;
    private $firstHeaderWritten;
    private $firstFooterWritten;

    public function setSection($section)
    {
        $this->section = $section;
        $this->firstHeaderWritten = false;
        $this->firstFooterWritten = false;
    }

    public function Header()
    {
        if ($this->section == null)
            return;

        $headers = $this->section->getHeaders();
        if (count($headers) == 0)
            return;

        $style = $this->section->getSettings();

        $this->SetY(Size::tocm($style->getMarginTop()));

        if ($this->firstHeaderWritten)
            $header = $this->regularHF($headers);
        else {
            $header = $this->firstPageHF($headers);
            $this->firstHeaderWritten = true;
        }


        if ($header != null) {
            $tm = 0;
            (new Container($header))->write($this, $tm);

            $this->SetTopMargin($this->GetY());
        }
    }

    private function regularHF($headers_footers)
    {
        foreach ($headers_footers as $hf)
            if ($hf->getType() != Footer::FIRST)
                return $hf;
        return $null;
    }

    private function firstPageHF($headers_footers)
    {
        foreach ($headers_footers as $hf)
            if ($hf->getType() == Footer::FIRST)
                return $hf;
        foreach ($headers_footers as $hf)
            return $hf;
        return null;
    }

    public function Footer()
    {
        if ($this->section == null)
            return;

        $footers = $this->section->getFooters();
        if (count($footers) == 0)
            return;

        if ($this->firstFooterWritten)
            $footer = $this->regularHF($footers);
        else {
            $footer = $this->firstPageHF($footers);
            $this->firstFooterWritten = true;
        }

        if ($footer != null) {
            $style = $this->section->getSettings();

            $container = new Container($footer);
            $this->startTransaction();
            $this->SetY(0);
            $tm = 0;
            $container->write($this, $tm);
            $height = $this->GetY();
            $this->rollbackTransaction(true);

            $this->SetY($this->getPageHeight() - Size::tocm($style->getMarginBottom()) - $height);

            $tm = 0;
            $container->write($this, $tm);
        }
    }

}


class PDF2 extends AbstractWriter
{


    public function __construct(PhpWord $phpWord)
    {
        $this->phpWord = $phpWord;
    }


    public function save($filename = null)
    {
        $pdf = new MyPDF('P', 'cm', 'A4');

        $pdf->SetCellPadding(0);

        $tm = 0;
        foreach ($this->phpWord->getSections() as $section)
            (new Section($section))->write($pdf, $tm);

        $f = $this->openFile($filename);
        $this->writeFile($f, $pdf->output($filename, 'S'));

        return;
    }

}
