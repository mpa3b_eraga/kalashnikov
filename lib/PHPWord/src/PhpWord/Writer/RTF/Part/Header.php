<?php
/**
 * This file is part of PHPWord - A pure PHP library for reading and writing
 * word processing documents.
 *
 * PHPWord is free software distributed under the terms of the GNU Lesser
 * General Public License version 3 as published by the Free Software Foundation.
 *
 * For the full copyright and license information, please read the LICENSE
 * file that was distributed with this source code. For the full list of
 * contributors, visit https://github.com/PHPOffice/PHPWord/contributors.
 *
 * @link        https://github.com/PHPOffice/PHPWord
 * @copyright   2010-2014 PHPWord contributors
 * @license     http://www.gnu.org/licenses/lgpl.txt LGPL version 3
 */

namespace PhpOffice\PhpWord\Writer\RTF\Part;

use PhpOffice\PhpWord\Settings;
use PhpOffice\PhpWord\Shared\Drawing;
use PhpOffice\PhpWord\Style;
use PhpOffice\PhpWord\Style\Font;
use PhpOffice\PhpWord\Style\Numbering;

/**
 * RTF header part writer
 *
 * @since 0.11.0
 */
class Header extends AbstractPart
{
    /**
     * Font table
     *
     * @var array
     */
    private $fontTable = array();

    /**
     * Color table
     *
     * @var array
     */
    private $colorTable = array();


    private $numberingTable = array();

    /**
     * Get font table
     */
    public function getFontTable()
    {
        return $this->fontTable;
    }

    /**
     * Get color table
     */
    public function getColorTable()
    {
        return $this->colorTable;
    }



    public function getNumberingTable()
    {
        return $this->numberingTable;
    }


    /**
     * Write part
     *
     * @return string
     */
    public function write()
    {
        $this->registerFont();

        $content = '';

        $content .= $this->writeCharset();
        $content .= $this->writeDefaults();
        $content .= $this->writeFontTable();
        $content .= $this->writeColorTable();
        $content .= $this->writeNumberingStyles();
        $content .= $this->writeGenerator();
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Write character set
     *
     * @return string
     */
    private function writeCharset()
    {
        $content = '';

        $content .= '\ansi';
        $content .= '\ansicpg1252';
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Write header defaults
     *
     * @return string
     */
    private function writeDefaults()
    {
        $content = '';

        $content .= '\deff0';
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Write font table
     *
     * @return string
     */
    private function writeFontTable()
    {
        $content = '';

        $content .= '{';
        $content .= '\fonttbl';
        foreach ($this->fontTable as $index => $font) {
            $content .= "{\\f{$index}\\fnil\\fcharset0 {$font};}";
        }
        $content .= '}';
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Write color table
     *
     * @return string
     */
    private function writeColorTable()
    {
        $content = '';

        $content .= '{';
        $content .= '\colortbl';
        foreach ($this->colorTable as $color) {
            list($red, $green, $blue) = Drawing::htmlToRGB($color);
            $content .= ";\\red{$red}\\green{$green}\\blue{$blue}";
        }
        $content .= ';}';
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Write
     *
     * @return string
     */
    private function writeGenerator()
    {
        $content = '';

        $content .= '{\*\generator PhpWord;}'; // Set the generator
        $content .= PHP_EOL;

        return $content;
    }

    /**
     * Register all fonts and colors in both named and inline styles to appropriate header table
     */
    private function registerFont()
    {
        $phpWord = $this->getParentWriter()->getPhpWord();
        $this->fontTable[] = Settings::getDefaultFontName();

        // Search named styles
        $styles = Style::getStyles();
        foreach ($styles as $style) {
            $this->registerFontItems($style);
        }

        // Search inline styles
        $sections = $phpWord->getSections();
        foreach ($sections as $section) {
            $elements = $section->getElements();
            foreach ($elements as $element) {
                if (method_exists($element, 'getFontStyle')) {
                    $style = $element->getFontStyle();
                    $this->registerFontItems($style);
                }
            }
        }
    }

    /**
     * Register fonts and colors
     *
     * @param \PhpOffice\PhpWord\Style\AbstractStyle $style
     */
    private function registerFontItems($style)
    {
        $defaultFont = Settings::getDefaultFontName();
        $defaultColor = Settings::DEFAULT_FONT_COLOR;

        if ($style instanceof Font) {
            $this->registerFontItem($this->fontTable, $style->getName(), $defaultFont);
            $this->registerFontItem($this->colorTable, $style->getColor(), $defaultColor);
            $this->registerFontItem($this->colorTable, $style->getFgColor(), $defaultColor);
        }
    }

    /**
     * Register individual font and color
     *
     * @param array $table
     * @param string $value
     * @param string $default
     */
    private function registerFontItem(&$table, $value, $default)
    {
        if (in_array($value, $table) === false && $value !== null && $value != $default) {
            $table[] = $value;
        }
    }




    private function writeNumberingStyles()
    {
        $listtable = '{\*\listtable';
        $overridetable = '{\*\listoverridetable';

        $listid = 0;

        $this->numberingTable = array();

        foreach (Style::getStyles() as $style)
        {
            if (! $style instanceof Numbering)
                continue;

            $listid++;

            $this->numberingTable[$listid] = $style;

            $listtable .= "{\\list\\listid{$listid}\\listtemplateid{$listid}\n";

            foreach ($style->getLevels() as $level)
            {
                $listtable .= '{\listlevel';
                switch ($level->getFormat())
                {
                    case 'bullet': $listtable .= '\levelnfc23'; break;
                    case 'upperRoman': $listtable .= '\levelnfc1'; break;
                    case 'lowerRoman': $listtable .= '\levelnfc2'; break;
                    case 'upperLetter': $listtable .= '\levelnfc3'; break;
                    case 'lowerLetter': $listtable .= '\levelnfc4'; break;
                    default: $listtable .= '\levelnfc0';
                }
                switch ($level->getAlign())
                {
                    case 'center': $listtable .= '\leveljc1'; break;
                    case 'right': $listtable .= '\leveljc2'; break;
                    default: $listtable .= '\leveljc0'; break;
                }
                $listtable .= '\levelstartat' . $level->getStart();
                if ($level->getFormat() != 'bullet')
                {
                    $listtable .= "{\\leveltext \\'02\\'0{$level->getLevel()}.;}";
                    $listtable .= "{\\levelnumbers\\'01;}";
                }
                else
                {
                    switch ($level->getLevel() % 3)
                    {
                        case 0: $listtable .= "{\\leveltext \\'01\u9679?;}"; break;
                        case 1: $listtable .= "{\\leveltext \\'01\u9675?;}"; break;
                        case 2: $listtable .= "{\\leveltext \\'01\u9632?;}"; break;
                    }
                    $listtable .= "{\\levelnumbers;}";
                }
                $listtable .= '\li' . $level->getLeft();
                $listtable .= '\fi' . -$level->getHanging();
                $listtable .= '\tx' . $level->getTabPos();
                $listtable .= "}\n";
            }

            $listtable .= '}';

            $overridetable .= "{\\listoverride\\listid{$listid}\\listoverridecount0\\ls{$listid}}";
        }

        $listtable .= '}';
        $overridetable .= '}';

        return "\n" . $listtable . "\n" . $overridetable . "\n";
    }
}
