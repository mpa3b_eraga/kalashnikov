<?php

namespace PhpOffice\PhpWord\Writer\RTF\Element;

use PhpOffice\PhpWord\Element\Footer;


class Section extends Container
{

    public function write()
    {
        $content = '';

        $section = $this->element;
        $style = $section->getSettings();

        $content .= '\sectd ';

        $content .= '\pgwsxn' . $style->getPageSizeW();
        $content .= '\pghsxn' . $style->getPageSizeH();
        $content .= '\margl' . $style->getMarginLeft();
        $content .= '\margr' . $style->getMarginRight();
        $content .= '\margt' . $style->getMarginTop();
        $content .= '\margb' . $style->getMarginBottom();

        $content .= '\headery' . $style->getHeaderHeight();

        foreach ($section->getHeaders() as $header)
            if ($header->getType() == Footer::FIRST)
            {
                $content .= '\titlepg ';
                break;
            }

        foreach ($section->getHeaders() as $header)
        {
            $content .= '{\header';
            if ($header->getType() == Footer::FIRST)
                $content .= 'f';
            $content .= ' ';
            $content .= (new Container($this->parentWriter, $header))->write();
            $content .= '}';
        }

        foreach ($section->getFooters() as $footer)
        {
            $content .= '{\footer';
            if ($footer->getType() == Footer::FIRST)
                $content .= 'f';
            $content .= ' ';
            $content .= (new Container($this->parentWriter, $footer))->write();
            $content .= '}';
        }

        $content .= parent::write();

        return $content;
    }

}
