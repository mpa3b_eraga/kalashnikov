<?php

namespace PhpOffice\PhpWord\Writer\RTF\Element;


class PreserveText extends Text
{

    protected function getText()
    {
        $text = '';

        foreach ($this->element->getText() as $part)
        {
            if ($part[0] == '{')
            {
                if (preg_replace('/^\{\s*|\s*}$/', '', $part) == 'PAGE')
                {
                    $text .= '\chpgn';
                    continue;
                }
            }

            $text .= $part;
        }

        return $text;
    }

}
