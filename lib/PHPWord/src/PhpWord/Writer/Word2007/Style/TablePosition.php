<?php

namespace PhpOffice\PhpWord\Writer\Word2007\Style;

class TablePosition extends AbstractStyle
{

    public function write()
    {
        $style = $this->getStyle();

        if ($style !== null)
        {
            $xmlWriter = $this->getXmlWriter();
            $xmlWriter->startElement('w:tblpPr');
            foreach ($style as $key => $value)
                $xmlWriter->writeAttribute('w:' . $key, $value);
            $xmlWriter->endElement(); // w:tblpPr
        }
    }

}
