<?php

require_once 'lib/PHPWord/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();


require_once('DocTemplate.php');


class Minutes extends DocTemplate
{

    public function getTitle()
    {
        return 'Протокол';
    }

    public function generateDocument($params)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection(array(
            'headerHeight' => $this->cm(1.7),
            'marginTop' => $this->cm(1.7),
            'marginBottom' => $this->cm(1.2),
            'marginLeft' => $this->cm(2.45),
            'marginRight' => $this->cm(1.8),
        ));


        $this->createHeader($section);
        $this->createFooter($section);


        $this->createStyles($phpWord);


        $section->addText('АКЦИОНЕРНОЕ ОБЩЕСТВО', 'company_info_bold', 'company_info_p');
        $section->addText('(АО «КОНЦЕРН «КАЛАШНИКОВ»)', 'company_info_bold', 'company_info_p');

        $section->addTextBreak(1, 'body', 'body_p');
        $section->addText('ПРОТОКОЛ', 'name', 'name_p');

        $number = '_______________';
        $number_date = '______________';
        $section->addText("$number_date № $number", 'number', array(
            'spaceBefore' => 360,
            'spaceAfter' => 360,
            'lineHeight' => 1.15
        ));

        $section->addText('г. Ижевск', 'body', array(
            'lineHeight' => 1.15,
        ));
        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell($this->cm(7.5));
        $cell->addText($params['subject'], 'subject_bold', array(
            'spaceBefore' => 360,
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
        ));

        $parts = explode('.', $params['chairman-name']);
        if (count($parts) == 3) {
            $params['chairman-name'] = trim($parts[0]) . '. ' . trim($parts[1]) . '. ' . trim($parts[2]);
        }

        $section->addText('Время: ' . $params['time'], 'subject', 'subject_p');
        $section->addText('Место: ' . $params['place'], 'subject', 'subject_p');
        $section->addText('Председатель: ' . $params['chairman-name'] . ' — ' . mb_strtolower($params['chairman-position'], 'UTF-8'), 'subject', 'subject_p');
        $section->addText('Секретарь: ' . $params['secretary'], 'subject', 'subject_p');
        $section->addText('Присутствовали: ' . $params['attendants'], 'subject', array(
            'spaceBefore' => $this->cm(0.35),
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
        ));
        $section->addText('Повестка дня:', 'body', 'body_p');
        $this->addHTMLText($phpWord, $section, $params['agenda'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $section->addText('СЛУШАЛИ:', 'body', 'orders_title');
        $this->addHTMLText($phpWord, $section, $params['hear'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 240,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $section->addText('ВЫСТУПИЛИ:', 'body', 'orders_title');
        $this->addHTMLText($phpWord, $section, $params['talk'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 240,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $section->addText('РЕШИЛИ:', 'body', 'orders_title');
        $this->addHTMLText($phpWord, $section, $params['result'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 240,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));

        $this->setLastParagraphStyle($section, 'body_last_p');

        $section->addTextBreak(1, 'body', 'body_p');
        $table = $section->addTable();
        if (!empty($params['signee-1-name'])) {
            $table->addRow();
            $cell = $table->addCell($this->cm(8.4));
            $cell->addTextBreak(1, 'body', 'body_p');
            $cell->addText($params['signee-1-name'], 'signee', 'signee_p');
            $cell->addText($params['signee-1-position'], 'signee', 'signee_p');
            $table->addCell($this->cm(1));
        }
        if (!empty($params['signee-2-name'])) {
            $table->addRow();
            $cell = $table->addCell($this->cm(8.4));
            $cell->addTextBreak(1, 'body', 'body_p');
            $cell->addText($params['signee-2-name'], 'signee', 'signee_p');
            $cell->addText($params['signee-2-position'], 'signee', 'signee_p');
            $table->addCell($this->cm(1));
        }

        return $phpWord;
    }

    protected function createStyles($phpWord)
    {
        parent::createStyles($phpWord);

        $phpWord->addFontStyle('subject_bold', array(
            'name' => 'Verdana',
            'size' => 10,
            'italic' => true,
            'bold' => true,
        ));

        $phpWord->addParagraphStyle('orders_title', array(
            'spaceBefore' => 240,
            'spaceAfter' => 200,
            'lineHeight' => 1.15,
        ));
    }

}
