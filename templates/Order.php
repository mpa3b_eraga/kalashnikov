<?php

require_once 'lib/PHPWord/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();


require_once('DocTemplate.php');


class Order extends DocTemplate
{

    public function getTitle()
    {
        return 'Приказ';
    }

    public function generateDocument($params)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection(array(
            'headerHeight' => $this->cm(1.7),
            'marginTop' => $this->cm(1.7),
            'marginBottom' => $this->cm(1.2),
            'marginLeft' => $this->cm(2.45),
            'marginRight' => $this->cm(1.8),
        ));


        $this->createHeader($section);
        $this->createFooter($section);


        $this->createStyles($phpWord);


        $section->addText('АКЦИОНЕРНОЕ ОБЩЕСТВО', 'company_info_bold', 'company_info_p');
        $section->addText('(АО «КОНЦЕРН «КАЛАШНИКОВ»)', 'company_info_bold', 'company_info_p');

        $section->addTextBreak(1, 'body', 'body_p');
        $section->addText('ПРИКАЗ', 'name', 'name_p');

        $number = '_______________';
        $number_date = '______________';
        $section->addText("$number_date № $number", 'number', array(
            'spaceBefore' => 360,
            'spaceAfter' => 360,
            'lineHeight' => 1.15
        ));

        $section->addText('г. Ижевск', 'body', array(
            'lineHeight' => 1.15,
        ));
        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell($this->cm(7.5));
        $cell->addText($params['subject'], 'subject_bold', array(
            'spaceBefore' => 360,
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
        ));


        $this->addHTMLText($phpWord, $section, $params['facts'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $section->addText('ПРИКАЗЫВАЮ:', 'body', array(
            'align' => 'justify',
            'spaceAfter' => 330,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
        ));
        $this->addHTMLText($phpWord, $section, $params['orders'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));


//        $exists = 0;
//        for ($i = 0; $i < count($params['app-name']); $i++) {
//            if (!empty($params['app-name'][$i])) {
//                $exists += 1;
//            }
//        }
//        if ($exists) {
//            $table = $section->addTable();
//            $table->addRow();
//            $cell = $table->addCell($this->cm(16.8));
//            if ($exists > 1) {
//                $cell->addTextBreak(1, 'body', 'body_p');
////            $cell->addText('Приложения:', 'body', 'body_p');
//                for ($i = 0; $i < count($params['app-name']); $i++) {
//                    if (empty($params['app-name'][$i])) continue;
//                    $cell->addText('Приложение №'.$i.': '.$params['app-name'][$i].' (на '.$params['app-volume'][$i].' л. в '.$params['app-copies'][$i].' экз.)', 'body', 'body_p');
//                }
//            } else {
//                if ($params['app-name'][1]) {
//                    $cell->addTextBreak(1, 'body', 'body_p');
//                    $cell->addText('Приложение: '.$params['app-name'][1].' (на '.$params['app-volume'][1].' л. в '.$params['app-copies'][1].' экз.)', 'body', 'body_p');
//                }
//            }
//        }

        $section->addTextBreak(2, 'body', 'body_p');
        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell($this->cm(8.4));
        $cell->addText($params['signee-name'], 'signee', 'signee_p');
        $cell->addText($params['signee-position'], 'signee', 'signee_p');
        $table->addCell($this->cm(1));

        return $phpWord;
    }

    protected function createStyles($phpWord)
    {
        parent::createStyles($phpWord);

        $phpWord->addFontStyle('subject_bold', array(
            'name' => 'Verdana',
            'size' => 10,
            'italic' => true,
            'bold' => true,
        ));

        $phpWord->addParagraphStyle('orders_title', array(
            'spaceBefore' => 240,
            'spaceAfter' => 200,
            'lineHeight' => 1.15,
        ));
    }

}
