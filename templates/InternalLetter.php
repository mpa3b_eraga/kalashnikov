<?php

require_once 'lib/PHPWord/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();


require_once('DocTemplate.php');


class InternalLetter extends DocTemplate
{

    public function getTitle()
    {
        return 'Внутреннее служебное письмо';
    }

    public function getShortTitle()
    {
        return 'Внутреннее письмо';
    }


    public function generateDocument($params)
    {
        $phpWord = new \PhpOffice\PhpWord\PhpWord();

        $section = $phpWord->addSection(array(
            'headerHeight' => $this->cm(1.7),
            'marginTop' => $this->cm(1.7),
            'marginBottom' => $this->cm(1.2),
            'marginLeft' => $this->cm(2.45),
            'marginRight' => $this->cm(1.8),
        ));


        $this->createHeader($section);
        $this->createFooter($section);


        $this->createStyles($phpWord);


        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell($this->cm(8.4));

        $cell->addText('АКЦИОНЕРНОЕ ОБЩЕСТВО', 'company_info_bold', 'company_info_p');
        $cell->addText('(АО «КОНЦЕРН «КАЛАШНИКОВ»)', 'company_info_bold', 'company_info_p');

        $cell = $table->addCell($this->cm(8.4));
        $cell->addText($params['receiver-position'], 'receiver', 'receiver_p');
        $cell->addTextBreak(1, 'receiver', 'receiver_p');
        $cell->addText($params['receiver-name'], 'receiver', 'receiver_p');

        $number = '_______________';
        $number_date = '______________';
        $reply_number = !empty($params['reply-number']) ? $params['reply-number'] : '__________';
        $reply_number_date = !empty($params['reply-number-date']) ? $params['reply-number-date'] : '______________';
        $section->addText("$number_date № $number", 'number', array('spaceBefore' => $this->cm(0.5), 'spaceAfter' => 0, 'lineHeight' => 1.5));
        $section->addText("На № $reply_number от $reply_number_date", 'number', array('spaceAfter' => 0, 'lineHeight' => 1.5));

        $table = $section->addTable();
        $table->addRow();
        $cell = $table->addCell($this->cm(7.5));
        $cell->addText($params['subject'], array(
            'name' => 'Verdana',
            'size' => 10,
            'italic' => true,
            'bold' => true
        ), array(
            'spaceBefore' => $this->cm(0.35),
            'spaceAfter' => $this->cm(0.35),
            'lineHeight' => 1.15,
        ));

        $section->addTextBreak(1, 'greeting', 'body_p');
        $section->addText($params['greeting'], 'greeting', 'body_p');


        $this->addHTMLText($phpWord, $section, $params['body'], 'body', 'body_p', array(
            'align' => 'justify',
            'spaceAfter' => 360,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));


        $exists = 0;
        for ($i = 0; $i < count($params['app-name']); $i++) {
            if (!empty($params['app-name'][$i])) {
                $exists += 1;
            }
        }
        if ($exists) {
            $table = $section->addTable();
            $table->addRow();
            $cell = $table->addCell($this->cm(16.8));
            if ($exists > 1) {
                $cell->addText('Приложения:', 'body', 'body_p');
                for ($i = 0; $i < count($params['app-name']); $i++) {
                    if (empty($params['app-name'][$i])) continue;
                    $cell->addText($i . '. ' . $params['app-name'][$i] . ' (на ' . $params['app-volume'][$i] . ' л. в ' . $params['app-copies'][$i] . ' экз.).', 'body', 'body_p');
                }
            } else {
                if ($params['app-name'][1]) {
                    $cell->addText('Приложение: ' . $params['app-name'][1] . ' (на ' . $params['app-volume'][1] . ' л. в ' . $params['app-copies'][1] . ' экз.).', 'body', 'body_p');
                }
            }
        }


        $exists = 0;
        for ($i = 0; $i < count($params['signee-name']); $i++) {
            if (!empty($params['signee-name'][$i]) && !empty($params['signee-position'][$i])) {
                $exists += 1;
            }
        }
        if ($exists) {
            $section->addTextBreak(1, 'body', 'body_p');
            for ($i = 0; $i < count($params['signee-name']); $i++) {
                if (!empty($params['signee-name'][$i]) && !empty($params['signee-position'][$i])) {
                    $section->addTextBreak(1, 'body', 'body_p');
                    $table = $section->addTable();
                    $table->addRow();
                    $cell = $table->addCell($this->cm(8.4));
                    $cell->addText($params['signee-name'][$i], 'signee', 'signee_p');
                    $cell->addText($params['signee-position'][$i], 'signee', 'signee_p');
                    $table->addCell($this->cm(1));
                }
            }
        }


        $exists = false;
        for ($i = 0; $i < count($params['approve-name']); $i++) {
            if (!empty($params['approve-name'][$i])) {
                $exists = true;
            }
        }
        if ($exists) {
            $table = $section->addTable();
            $n = 0;
            for ($i = 0; $i < count($params['approve-name']); $i++) {
                if (!empty($params['approve-name'][$i])) {
                    if ($n % 2 == 0) {
                        $table->addRow();
                    }
                    $n += 1;
                    $cell = $table->addCell($this->cm(8.4));
                    $cell->addTextBreak(2, 'signee', 'signee_p');
                    $cell->addText('СОГЛАСОВАНО', 'signee', 'signee_p');
                    $cell->addText($params['approve-name'][$i], 'signee', 'signee_p');
                    $cell->addText($params['approve-position'][$i], 'signee', 'signee_p');
                    $cell->addText($params['approve-date'][$i], 'body', 'signee_p');
                }
            }
        }


        $tbl = $section->addTable(array(
            'position' => array(
                'vertAnchor' => 'margin',
                'tblpYSpec' => 'bottom',
            ),
        ));
        $tbl->addRow();
        $cell = $tbl->addCell($this->cm(16));
        $cell->addText($params['executive-name'], 'executive', 'executive_p');
        $cell->addText($params['executive-phone'], 'executive', 'executive_p');

        return $phpWord;
    }


//    protected function writeApprove($container, $name, $position, $date)
//    {
//        $container->addText('СОГЛАСОВАНО', 'signee', 'signee_p');
//        $container->addTextBreak(1, 'signee', 'signee_p');
//        $container->addText($name, 'signee', 'signee_p');
//        $container->addText($position, 'signee', 'signee_p');
//        $container->addText($date, 'body', 'signee_p');
//    }

}
