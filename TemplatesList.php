<?php

$template_names = array(
    'ExternalLetter',
    'Resolution',
    'Order',
    'Minutes',
    'Direction',
    'InternalLetter',
);


$Templates = array();
foreach ($template_names as $template) {
    require_once('templates/' . $template . '.php');
    $Templates[] = new $template;
}
