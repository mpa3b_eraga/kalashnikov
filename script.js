$(function () {
    $('a[href="fakelink"]').on('click', false)
});


$(function () {
    $('.format-button').on('click', function () {
        $(this).addClass('active').siblings().removeClass('active');
        $('form[name="document"] [name="format"]').val($(this).attr('value'))
    })
});


$(function () {
    $('button.download').on('click', function () {
        $('form[name="document"]').submit()
    })
});

var applyDatepickers = function () {
    $('.datepicker').datepicker({
        showOtherMonths: true,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true
    })
};


$(function () {
    applyDatepickers();
});


$(function () {

    function clean_clones(template) {
        $('.clone[data-template="' + template + '"]').each(function (i) {
            $(this).find('.clone-number').text(i + 1)
        })
    }

    $(document).on('click', '.clone-link', function (e) {
        e.preventDefault();

        var $this = $(this);

        var template = $($this.data('template'));
        var clone = template.clone();
        clone
            .removeAttr('id').removeClass('template')
            .addClass('clone').attr('data-template', $this.data('template'));
        $this.before(clone);

        clean_clones($this.data('template'));

        clone.find('.template-ckeditor')
            .removeClass('template-ckeditor')
            .addClass('ckeditor')
            .ckeditor();

        if (clone.offset().top + clone.outerHeight() >= $('body').scrollTop() + window.outerHeight) {
            $('html, body').animate({
                scrollTop: clone.offset().top - clone.css('margin-top').replace('px', '')
            }, 750)
        }

        clone.find('.datepick').addClass('datepicker');
        applyDatepickers();
    });

    $(document).on('click', '.clone-delete', function (e) {
        e.preventDefault();

        var $this = $(this);
        var clone = $this.parent();

        clone.remove();

        clean_clones(clone.attr('data-template'))
    })
});


$(function(){

    CKEDITOR.plugins.add('li_nums', {
        init: function(editor) {
            var max = 100;
            var style = '<style type="text/css">';
            for (var i=1; i<=max; i++) {
                style = style + 'ol > li.li_num_' + i + ':before { content: "' + i + '. "; } '
            }
            style = style + '</style>';
            var styles_init = false;
            editor.on('change', function() {
                $('iframe').each(function() {
                    var num = 0;
                    if (!styles_init) {
                        $(this).contents().find('head').append(style);
                        styles_init = true;
                    }
                    $(this).contents().find('body li').each(function() {
                        for (var i=1; i<=max; i++) {
                            $(this).removeClass('li_num_'+i);
                        }
                    });
                    $(this).contents().find('body > *').each(function() {
                        var tag = $(this).get(0).tagName;
                        switch (tag) {
                            case 'P':
                                for (var i=1; i<=max; i++) {
                                    $(this).removeClass('li_num_'+i);
                                }
                                num = 0;
                                break;
                            case 'OL':
                                $(this).children('li').each(function () {
                                    num = num + 1;
                                    for (var i=1; i<=max; i++) {
                                        $(this).removeClass('li_num_'+i);
                                    }
                                    $(this).addClass('li_num_'+num);
                                });
                                break;
                        }
                    });
                });
            });
        }
    });

});
