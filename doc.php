<?php

ini_set('display_startup_errors', 1);
ini_set('display_errors', 1);
error_reporting(-1);

require_once 'lib/PHPWord/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();


require_once('SimpleHTMLParser.php');


$templateName = $_REQUEST['template'];
require_once('templates/' . $templateName . '.php');
$template = new $templateName();

$phpWord = $template->generateDocument($_REQUEST);


$attachment = true;

if (!isset($_REQUEST['number'])) {
    $_REQUEST['number'] = '';
}

if (!isset($_REQUEST['number-date'])) {
    $_REQUEST['number-date'] = date('Y-m-d_H-i-s');
}


switch ($_REQUEST['template']) {
    case 'ExternalLetter':
        $name = 'Исходящее письмо - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    case 'Resolution':
        $name = 'Постановление - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    case 'Order':
        $name = 'Приказ - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    case 'Minutes':
        $name = 'Протокол - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    case 'Direction':
        $name = 'Распоряжение - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    case 'InternalLetter':
        $name = 'Внутреннее письмо - ' . $_REQUEST['subject'] . ' №' . $_REQUEST['number'] . ' ' . $_REQUEST['number-date'];
        break;
    default:
        $name = 'doc';
        break;
}


switch ($_REQUEST['format']) {
    case 'pdf':
        $writerName = 'PDF2';
        $mime = 'application/pdf';
        $filename = $name . '.pdf';
        $attachment = false;
        break;

    case 'rtf':
        $writerName = 'RTF';
        $mime = 'text/rtf';
        $filename = $name . '.rtf';
        break;

    default:
        $writerName = 'Word2007';
        $mime = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        $filename = $name . '.docx';
        break;
}


$objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, $writerName);
$tmpfilename = tempnam(sys_get_temp_dir(), 'doc-');
$objWriter->save($tmpfilename);

$content = file_get_contents($tmpfilename);
unlink($tmpfilename);

header('Content-Type: ' . $mime);
if ($attachment)
    header('Content-Disposition: attachment; filename=' . $filename);
header('Content-Length: ' . strlen($content));
echo $content;
