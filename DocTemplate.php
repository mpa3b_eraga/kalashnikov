<?php

require_once 'lib/PHPWord/src/PhpWord/Autoloader.php';
\PhpOffice\PhpWord\Autoloader::register();


abstract class DocTemplate
{

    public function getShortTitle()
    {
        return $this->getTitle();
    }

    public abstract function getTitle();

    public function getThumbnailURL()
    {
        return '/templates/' . get_class($this) . '.png';
    }

    public function getHTMLForm()
    {
        return file_get_contents('templates/' . get_class($this) . '.html');
    }

    public abstract function generateDocument($params);


    protected function createStyles($phpWord)
    {
        $phpWord->addParagraphStyle('logo_p', array(
            'spaceAfter' => 200,
        ));
        $phpWord->addFontStyle('company_info_bold', array(
            'name' => 'Verdana',
            'size' => 8,
            'bold' => true,
        ));
        $phpWord->addFontStyle('company_info', array(
            'name' => 'Verdana',
            'size' => 8,
        ));
        $phpWord->addParagraphStyle('company_info_p', array(
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'spaceAfter' => 0,
        ));
        $phpWord->addFontStyle('receiver', array(
            'name' => 'Verdana',
            'size' => 11,
            'bold' => true,
        ));
        $phpWord->addParagraphStyle('receiver_p', array(
            'spaceBefore' => 0,
            'spaceAfter' => 0,
        ));
        $phpWord->addFontStyle('number', array(
            'name' => 'Verdana',
            'size' => 11,
        ));
        $phpWord->addFontStyle('subject', array(
            'name' => 'Verdana',
            'size' => 10,
            'italic' => true,
        ));
        $phpWord->addParagraphStyle('subject_p', array(
            'spaceBefore' => $this->cm(0.35),
            'spaceAfter' => $this->cm(0.35),
            'lineHeight' => 1.15,
        ));
        $phpWord->addFontStyle('greeting', array(
            'name' => 'Verdana',
            'size' => 11,
            'bold' => true,
        ));
        $phpWord->addFontStyle('name', array(
            'name' => 'Verdana',
            'size' => 11,
            'bold' => true,
        ));
        $phpWord->addParagraphStyle('name_p', array(
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'lineHeight' => 1.15,
        ));
        $phpWord->addFontStyle('body', array(
            'name' => 'Verdana',
            'size' => 11
        ));
        $phpWord->addParagraphStyle('body_p', array(
            'align' => 'justify',
            'spaceAfter' => 200,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
        ));
        $phpWord->addParagraphStyle('body_last_p', array(
            'align' => 'justify',
            'spaceAfter' => 240,
            'lineHeight' => 1.15,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $phpWord->addFontStyle('signee', array(
            'name' => 'Verdana',
            'size' => 11,
            'bold' => true,
        ));
        $phpWord->addParagraphStyle('signee_p', array(
            'spaceAfter' => 900,
            'lineHeight' => 3,
            'spaceBefore' => 0,
            'keepNext' => true,
        ));
        $phpWord->addFontStyle('page_no', array(
            'name' => 'Verdana',
            'size' => 8,
        ));
        $phpWord->addParagraphStyle('page_no_p', array(
            'align' => 'right',
            'spaceAfter' => 0,
        ));
        $phpWord->addFontStyle('executive', array(
            'name' => 'Verdana',
            'size' => 9,
            'color' => '5A5A5A',
        ));
        $phpWord->addParagraphStyle('executive_p', array(
            'spaceBefore' => 100,
        ));
    }

    public function cm($cm)
    {
        return (int)\PhpOffice\PhpWord\Shared\Font::centimeterSizeToTwips($cm);
    }

    protected function createHeader($section)
    {
        $header = $section->addHeader();
        $header->firstPage();
        $tr = $header->addTextRun('logo_p');
        $tr->addImage('logo.png', array(
            'width' => 4.5 / 2.5 * 96,
        ));

        $header = $section->addHeader();
        $tr = $header->addTextRun('logo_p');
        $tr->addImage('logo.png', array(
            'width' => 4.5 / 2.5 * 96,
        ));
        $header->addTextBreak(2, array(), 'logo_p');
    }

    protected function createFooter($section)
    {
        $section->addFooter()->firstPage();
        $footer = $section->addFooter();
        $footer->addPreserveText(' {PAGE}', 'page_no', 'page_no_p');
    }

    protected function addHTMLText($phpWord, $container, $html, $fontStyle, $parStyle, $lastParStyle = null)
    {
        (new SimpleHTMLParser())->parse($html, $phpWord, $container, $fontStyle, $parStyle);

        if ($lastParStyle != null)
            $this->setLastParagraphStyle($container, $lastParStyle);
    }

    protected function setLastParagraphStyle($container, $lastParStyle)
    {
        if ($lastParStyle != null) {
            $children = $container->getElements();
            for ($i = count($children) - 1; $i >= 0; $i--) {
                $el = $children[$i];

                if (method_exists($el, 'setParagraphStyle'))
                    $el->setParagraphStyle($lastParStyle);

                if (method_exists($el, 'getText')) {
                    $trimmed = trim($el->getText());
                    if ($trimmed == '' || $trimmed == "\xC2\xA0")
                        continue;
                }

                break;
            }
        }
    }

}
